import logging
import sys
import os
import time
import smtplib
import ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import pika
import json
from types import SimpleNamespace

# Set up logging
try:
    desired_log_level = os.environ.get("LOG_LEVEL")
    VaildLogLevels = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
    if desired_log_level not in VaildLogLevels or not desired_log_level:
        logging.basicConfig(
            format="%(asctime)s - %(message)s",
            level=logging.INFO)
        logging.info(
            "Logging initialized with invalid or no log level set. Defaulting to INFO."
        )
    else:
        logging.basicConfig(
            format="%(asctime)s - %(message)s",
            level=logging.getLevelName(desired_log_level),
        )
        logging.debug(
            "Logging initialized. Desired log level: %s" %
            desired_log_level)
except BaseException:
    sys.exit(1)

ServerStartTime = time.time()
logging.debug("Server start time: %s" % ServerStartTime)

sender_email = os.environ.get("MAIL_FROM_ADDRESS")
password = os.environ.get("SMTP_PASSWORD")
rabbitmq_address = os.environ.get("RABBITMQ_ADDRESS")
rabbitmq_port = os.environ.get("RABBITMQ_PORT")
rabbitmq_vhost = os.environ.get("RABBITMQ_VHOST")
rabbitmq_user = os.environ.get("RABBITMQ_USER")
rabbitmq_pass = os.environ.get("RABBITMQ_PASSWORD")


def send_activation_email(email, activation_code, username):
    message = MIMEMultipart("alternative")
    message["Subject"] = "Activate your SHIX account!"
    message["From"] = sender_email
    message["To"] = email

    # Create the plain-text and HTML version of your message
    text = f"""
    Greetings {username}! Thank you for registering an account with shix. Please click the following link to activate your account. https://api-dev.shix.app/activate/{activation_code}
    """
    html = f"""
    <html>
      <body>
      <center>
        <h1>Greetings {username}!</h1>
        <p>Thanks for registering an account with SHIX!</b>
        <br>
        <p>To complete your registration, please click the link below to activate your account.</p>
        <br>
        <button><a href="https://api-dev.shix.app/activate/{activation_code}">Activate Account</a></button>
        <br>
        <br>
        <p>If you did not register for an account with SHIX, please ignore this email.</b>
        <br>
        <p>Having trouble viewing this email? Please visit https://api-dev.shix.app/activate/{activation_code}</b>
      </center>
      </body>
    </html>
    """

    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)
    message.attach(part2)

    # Create secure connection with server and send email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(os.environ.get("SMTP_HOST"), os.environ.get("SMTP_PORT"), context=context) as server:
        server.ehlo()
        server.login(sender_email, password)
        server.sendmail(
            sender_email, email, message.as_string()
        )
        logging.info("Email sent successfully!")


def main():
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            host=rabbitmq_address,
            virtual_host=rabbitmq_vhost,
            credentials=pika.PlainCredentials(
                rabbitmq_user,
                rabbitmq_pass)))
    channel = connection.channel()

    channel.queue_declare(queue='shix-notify')

    def callback(ch, method, properties, body):
        job = json.loads(body, object_hook=lambda d: SimpleNamespace(**d))
        logging.info(f" [x] Received new job: {job.type}")
        if job.type == "activation":
            logging.info(f" [x] Sending activation email to {job.email}")
            send_activation_email(job.email, job.activationcode, job.username)

    channel.basic_consume(
        queue='shix-notify',
        on_message_callback=callback,
        auto_ack=True)

    print(' [*] Waiting for messages.')
    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
