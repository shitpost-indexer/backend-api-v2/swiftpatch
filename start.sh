#! /bin/bash
# Check if we are in the right directory
usage() {
    echo "Usage: $0 [-s] [-u] [-r] [-h]"
    echo "  -s  Start the server"
    echo "  -u  Update the server from git"
    echo "  -r  Restart the server if it crashes"
    echo "  -h  Display this help message"
}

# Check if any arguments were passed
if [ $# -eq 0 ]; then
    usage
    exit 0
fi 

# Verify that the script is in the directory
if [ ! -f "main.py" ]; then
    echo "Please run this script from the services's root directory"
    exit 0
fi

# Verify that we have the .env file
if [ ! -f ".env" ]; then
    echo "Please create a .env file"
    exit 0
fi

# Verify we have python3
if ! command -v python3 &> /dev/null
then
    echo "python3 could not be found"
    exit 0
fi


start=false
update=false
restart=false

while getopts "shp:ur" OPTION; do
  case "$OPTION" in
    s)
      start=true
      echo "Starting server..."
      ;;
    u)
      update=true
      ;;
    r)
      restart=true
      echo "Server will restart if it crashes"
      ;;
    h | * | ?)
      usage
      exit 0
      ;;
  esac
done

if [ ${start} == "true" ] && [ ${update} == "true" ]; then
    echo "Please specify either -s or -u, not both"
    exit 0
fi

if [ ${restart} == "true" ] && [ ${start} == "false" ]; then
    echo "Please specify -s to use -r"
    exit 0
fi

if [ ${restart} == "true" ] && [ ${update} == "true" ]; then
    echo "Cannot use -r with -u"
    exit 0
fi

# Check if started with --update then quit
if [ ${update} == "true" ]; then
    echo "Updating from git..."
    git pull
    exit 0
fi

if [ ${start} == "true" ]; then
    # Import the environment variables
    echo "Importing environment variables..."
    export $(<.env grep -v "^#" | xargs)
    if [ ${restart} == "true" ]; then
        while true; do
            echo "Starting Swiftpatch now..."
            python3 main.py
        done
    else
        echo "Starting Swiftpatch now..."
        python3 main.py
    fi
else
    exit 0
fi